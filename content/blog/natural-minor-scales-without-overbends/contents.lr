title: Natural minor scales that don't require overbends
---
pub_date: 2018-11-28
---
author: lsrdg
---
body:

This post in an overview of the natural minor scales that can be played on a diatonic
harmonica without overbends, thus you don't need to know what overbends are in
order to follow along. 

If you don't know what all of this is about, [read this
first](/blog/scales-without-overbends). That's introduction to the topic.

In a standard 10 holes diatonic harmonica, it is possible to get at least 
one octave of three different natural minor scales _without the use of overbends_. 
Assuming a `C harmonica`, you can play the natural minor scales of `D`, `A` and `E`.

By the way, let's keep assuming an `C harmonica`. And the notes with a light
yellow background are those belonging to the scale.

## 4th position: A

If you are following along, the [last
article](/blog/major-scales-without-overbends) talked about playing the major
scale in first position (or straight harp). All major scale has its [relative
minor scale](https://en.wikipedia.org/wiki/Relative_key). E.g, if you play `C
major` scale, but starting from the 6th note (in this case `A`), you'll be
playing an `A natural minor scale`. That's it, at least two for the price of
one.

If the last paragraph doesn't make sense, take a look on the image below and
play along:


![A natural minor - C diatonic harmonica](a-natural-minor-c-diatonic-harmonica.png)

As you can see, that's almost a `C major scale` and you actually have two full
octaves, plues some remaining notes on the lower and higher octaves.

## 3rd position: D

Despite of it being traditionally a minor position (as [dorian
mode](https://en.wikipedia.org/wiki/Dorian_mode)), there is a whole natural
minor scale on the lower octave:

![D natural minor - C diatonic harmonica](d-natural-minor-c-diatonic-harmonica.png)

Taking a look at the image above, you'll will probably see notes that show up
quite often. The third (`F`) and the fifth (`A`) notes are usually important
notes, anything here will sound awful with bad tone and bad bends: this is a
great scale to practice these.


## 5th position: E

This position makes strong use of bends, and is usually set aside (I don't know
why). In the lower octave, (from `2` to `5`) there is a whole octave a natural
minor scale.

![E natural minor - C diatonic harmonica](e-natural-minor-c-diatonic-harmonica.png)

Probably nothing you'll be using all the time, but definitely gives a lot of
flexibility whenever needed.
